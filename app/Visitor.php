<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $fillable = [
        'title',
        'firstname',
        'middlename',
        'lastname',
        'gender',
        'nationality',
        'phone',
        'residence',
        'email_add',
        'home_reg',
        'roles'
    ];
}
