<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sermon extends Model
{
    protected $fillable = ['title','vid_file','author','description','date_uploaded'];

}
