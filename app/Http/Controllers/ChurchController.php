<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChurchController extends Controller
{
    public function showChurchList(){
        return view('superadmin.churcheslist');
    }

    public function viewNationalSum(){
        return view('');
    }

    public function viewTerritorySum(){
        return view('');
    }

    public function viewAreaSum(){
        return view('');
    }

    public function viewCircuitSum(){
        return view('');
    }

    public function addFamilyGroup(){
        return view('');
    }

    public function viewFamilyGroup(){
        return view('');
    }
}
