<?php

namespace App\Http\Controllers\AssemblyController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sermon;
use Session;

class SermonController extends Controller
{
    public function addSermon(){
        return view('assembly.sermon');
    }
    public function saveSermon(Request $request){
    //return['mm'=>$request->all(),'see'=>$request->has('files')];
ini_set('post_max_size','1G');
ini_set('upload_max_size','1G');
       $filename = null;
        if ($file = $request->file('files')) {
           // foreach($files as $file){
                $name = $file->getClientOriginalName();
                $filename = str_random(25).$name;
                $file->move('sermons', $filename); 
            // }
          
        }
        $save = new Sermon();
        $save->title = $request->title;
        $save->author = $request->author;
        $save->vid_file = $filename;
        $save->description = $request->description;
        $save->date = $request->date_uploaded;
       // $save->save();

        if($save->save()){
               // Session::flash('success','File Uploaded Successfully');
               // return back();
               return ['status'=>'true'];
            }else{
                return ['status'=>'false'];
               // Session::flash('failed','File Upload Failed');
                //return back();
            }
       
    }

    public function listSermon(){
        $listsermon = DB::table('sermons')->orderBy('created_at', 'desc')->get();
        return view('assembly.sermonlist', compact('list'));
    }
















}
