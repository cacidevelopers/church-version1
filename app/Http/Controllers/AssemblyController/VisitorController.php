<?php

namespace App\Http\Controllers\AssemblyController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Visitor;
use Session;

class VisitorController extends Controller
{
    public function addNewVisitor(){
        return view('assembly.addnewvisitor');
    }
    public function saveVisitorInformation(Request $request){
        $save = New Visitor();
        $save->title = $request->title;
        $save->firstname = $request->firstname;
        $save->middlename = $request->middlename;
        $save->lastname = $request->lastname;
        $save->gender = $request->gender;
        $save->nationality = $request->nationality;
        $save->phone = $request->phone;
        $save->residence = $request->residence;
        $save->email_add = $request->email_add;
        $save->home_reg = $request->home_reg;
        $save->roles = $request->roles;

        $save->save();
        Session::flash('success','Visitor Added Successfully');
            return back();
                Session::flash('error','Visitor Registration failed, please try again');
       return back();
    }
}
