<?php

namespace App\Http\Controllers\AssemblyController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;
use DB;
use Session;


class MemberController extends Controller
{
    public function addNewMember(){
        //code here
        return view('assembly.addnewmember');
    }

    public function memberslist(){
        $list = DB::table('members')->get();
        return view('assembly.memberslist', compact('list'));

    }
    public function profile(Request $id){
        
        return view('assembly.profile');
    }
    public function saveMemberInformation(Request $request){
        $save = Member::create($request->all());
        // dd($save->firstname);

       Session::flash('success','Member Added Successfully'); 
       return back();
    }
    public function deleteMember($id){
        $del = Member::find($id)->delete();
        if($del){
            return ['status'=>'success'];
        }else{
            return ['status'=>'failed'];
        }
    }

    public function editMember($id){
        $member = Member::find($id);
        return view('assembly.editmember', compact('member'));
    }
    
    public function saveMemberEditInformation(){


    }
}
