<?php

namespace App\Http\Controllers\AssemblyController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Expense;
use Session;

class ExpenseController extends Controller


{

    public function newImprest(){
        return view('assembly.imprest');
    }
    public function saveImprest(Request $request){
        $saveimprest = new Expense;
        $saveimprest->type = $request->type;
        $saveimprest->recipient = $request->receiver;
        $saveimprest->amount = $request->amount;
        $saveimprest->description = $request->description;
        $saveimprest->save();

            Session::flash('success','Expense Added Successfully');
            return back();
                Session::flash('error','Expense failed, please try again');
       return back();
    }

    public function expenseList(){
        $list = DB::table('expenses')->get();
        return view('assembly.expenselist', compact('list'));
    }
}
