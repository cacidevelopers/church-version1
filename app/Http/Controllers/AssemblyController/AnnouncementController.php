<?php

namespace App\Http\Controllers\AssemblyController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Announcement;
use Session;

class AnnouncementController extends Controller
{
    public function postAnnouncement(){
        return view('assembly.postannouncement');
    }

    public function saveAnnouncement(Request $r){
        $to = '';
        foreach($r->receivers as $receiver) 
            $to .= $receiver.' ';

     	$add = Announcement::create([
            'title' => $r->title,
            'description' => $r->description,
            'featured_image' => $r->featured_image,
            'school_id' => $r->school_id,
            'receiver' => $to
        ]);
     	if($add){
     		Session::flash('success','Announcement Posted Successfully');
     		return back();
     	}else{
     		Session::flash('error','Error Posting Announcement..Try Again');
     		return back();
     	}

     }
}
