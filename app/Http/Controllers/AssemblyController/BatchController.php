<?php

namespace App\Http\Controllers\AssemblyController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Group;
use DB;
use Session;
class BatchController extends Controller
{
    public function addGroup(){
        return view('assembly.addnewgroup');
    }
    public function saveGroup(Request $request){
        $memb = [];
        
        $addgroup = new Group;
        $addgroup->name = $request->name;
        $addgroup->group_leader_id = $request->group_leader;
        $addgroup->description = $request->description;
        if($addgroup->save()){
        if(count($request->memb)>0){
            foreach($request->memb as $member){
                DB::table('group_members')->insert([
                    'member_id'=>$member,
                    'group_id'=>$addgroup->id
                ]);
            }
        }
          Session::flash('success','New Group Added Successfully');
          return back();
        }else{
          Session::flash('error','Error Adding Group.. Try Again');
          return back();
        }
    }
}
