/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global $, window */


$(function () {


    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        // alert('ok');
        url: 'savesermon',
        multipart:true,
        replaceFileInput:false,
        disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
        maxFileSize: 99999999999,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|mp4)$/i
    });

   
    //console.log('here');

    $('#startupload').on('click',function(){
       // alert('uploading');   
       var form = $('.fileuploads');
       var formdata = false;
       if (window.FormData){
       formdata = new FormData(form[0]);
       }
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            type:'POST',
             url:"savesermon",
            data:formdata ? formdata : form.serialize(),
             processData: false,
            contentType: false,
            cache:false,
             success: function(data){
                if(data.status==='true'){
                  swal('Success','File upload successful','success');
                }else{
                    swal('Error','Error Uploading File','error');
                }
             },
             error:function(){

             }
            }).always(function () {
                $('#fileupload').removeClass('fileupload-processing');
            });

    })
});
