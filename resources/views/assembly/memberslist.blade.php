@extends('layouts.partials.mainlayout')
@section('css-above')
  <link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css)')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('assets/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  @endsection
@section('content')

<div class="box">
            <div class="box-header">
              <h3 class="box-title">Members List</h3>
            </div>
            <!-- /.box-header -->
                    <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="checkAll" id="checkAll"></th>
                                <th>#</th>
                                <th>Image</th>
                                <th>Member Name</th>
                                <th>Contact</th>
                                <th>Gender</th>
                                <th>Member ID</th>
                                <th>Occupation</th>
                                <th>Profession</th>
                                <th>Marital Status</th>
                                <th>Action</th>
                                </tr>
                        </thead>
                        <tbody>
                                @php
                                    $tabnum = 1;
                                @endphp
                                @foreach($list as $m)
                                <tr>
                               
                                    <td><input type="checkbox" name="NULL" value="{{$m->id}}"></td>
                                    <td>{{$tabnum++}}</td>

                                    <td>No image</td>
                                    <td>{{$m->firstname}} {{$m->middlename}} {{$m->lastname}}</td>
                                    <td>{{$m->phone}}</td>
                                    <td>{{$m->gender}}</td>
                                    <td>{{$m->user_id}}</td>
                                    <td>{{$m->occupation}}</td>
                                    <td>{{$m->profession}}</td>
                                    <td>{{$m->mar_status}}</td>

                                    

                                    <td>
                                        <a href="{{url('/member/profile/')}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-eye"></i> view</a>
                                        <a class="btn btn-outline-success btn-sm" href=""><i class="fa fa-edit"></i> edit</a>
                                        <a class="btn btn-outline-danger btn-sm delete" data="" href=""><i class="fa fa-trash"></i> delete</a>
                                    </td>
                                   
                                </tr> 
                                @endforeach
                            </tbody>
                    </table>
            </div>
          </div>

@endsection
@section('custom_scripts')
<script src="{{asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable()
  })
</script>
@endsection
