@extends('layouts.partials.mainlayout')
@section('css-above')
<style type="text/css">
     .error{
        color:red;
     }
 </style>
@endsection
@section('content')


@section('content')
<div class="row match-height">
    <div class="col-sm-12">
         @if(Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
            @endif
             @if(Session::has('error'))
            <div class="alert alert-danger">
                {{Session::get('error')}}
            </div>
            @endif
    </div>
        <div class="col-md-12">

            <div class="card">
                <!-- <div class="card-header"> -->
                    <!-- <h4 class="card-title" id="basic-layout-form">Add New Expenses</h4> -->
                    <!-- <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
                    </div> -->
                <!-- </div> -->
                <!-- <div class="card-content collapse show">
                    <div class="card-body"> -->
                        <div class="card-text">
                        </div>
                        <form class="form" id="addexpenseform" method="post" action="{{url('/saveimprest')}}">
                            {{csrf_field()}}
                            <div class="form-body">
                                <h2 class="form-section"><i class="ft-user"></i>Add Expenses</h2>

                                 <div class="row">
                                    <div class="col-md-6 col-md-offset-3">

                                        <div class="form-group">
                                            <label for="Type">Type</label>
                                             <input type="text" id="name" class="form-control" placeholder="type of expense" name="type" required="required">
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                 <div class="col-md-6 col-md-offset-3">
                                       <label for="date">Recipient</label>
                                    <input type="text" id="receiver" class="form-control " placeholder="Recipient" name="receiver" required="required">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                        <div class="form-group">
                                            <label for="amount">Amount </label>
                                             <input type="number" id="amount" class="form-control" placeholder="Amount Used" name="amount" required="required">
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                 <div class="col-md-6 col-md-offset-3">
                                        <div class="form-group">
                                            <label for="description">Description</label>
                                             <textarea rows="5" class="form-control" name="description" placeholder="Add Description"></textarea>
                                        </div>
                                    </div>
                                </div>
                  
                                           
                         </div>
                         <div class="col-md-6 col-md-offset-3">     
                            <div class="form-actions center">
                                <a href="{{url()->previous()}}" class="btn btn-warning mr-1">
                                    <i class="ft-x"></i> Cancel
                                </a>
                                <button type="submit" class="btn btn-primary" id="savebtn">
                                    <i class="fa fa-check-square-o"></i>Add Expenses <img src="{{asset('assets/images/loading.gif')}}" style="max-height: 20px;display: none;" id="loading">
                                </button>
                            </div>
                         </div>
                        </form>
                    </div>
                </div>
            <!-- </div>
        </div>
    </div> -->



















@endsection