@extends('layouts.partials.mainlayout')
@section('css-above')
<link rel="stylesheet" href="{{asset('/assets/bower_components/select2/dist/css/select2.min.css')}}">
<style>
    .form-control {
    border-radius: 0;
    box-shadow: none;
    border-color: #ef3939;
    padding: 25px;
}
</style>
@endsection
@section('content')

<div class="row match-height">
    <div class="col-sm-12">
         @if(Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
            @endif
             @if(Session::has('error'))
            <div class="alert alert-danger">
                {{Session::get('error')}}
            </div>
            @endif
    </div>
        <div class="col-md-12">

            <div class="card">
                <div class="row">
                  <div class="col-md-12">
                    <h1> Create New Group</h1>
                  </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="card-text">
                        </div>
                        <form class="form" id="addform" method="post" action="{{url('/savegroup')}}">
                           <input type="hidden" name="church_id" value="{{Session::get('church_id')}}" id="church_id">
                            {{csrf_field()}}
                            <div class="form-body">

                                 <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                             <input type="text" id="name" class="form-control" placeholder="Name of Group" name="name" required="required">
                                        </div>
                                    </div>
                                </div>  
                                <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                           <select class="form-control" name="group_leader" required="required">
                                             <option value="">Choose Group Leader</option>
                                             <?php $member = DB::table('members')->get(['id','firstname','lastname']); ?>
                                            @foreach($member as $mem)
                                            <option value="{{$mem->id}}">{{$mem->firstname}} {{$mem->lastname}}</option>
                                              @endforeach
                                           </select>
                                        </div>
                                        
                                </div>
                                 <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <div class="form-group">
                                            <label for="description">Description</label>
                                             <textarea rows="5" class="form-control" name="description" placeholder="Add Description"></textarea>
                                        </div>
                                    </div>
                                </div>

                                 <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <div class="form-group">
                                            <label for="name">Add Member to Group</label>
                                            <?php $member = DB::table('members')->get(['firstname','middlename','lastname','id']); ?>
                                            <select class="form-control select2-choice" multiple="multiple" name="memb[]">
                                              @foreach($member as $m)
                                                <option value="{{$m->id}}">Member:{{$m->id}}-{{$m->firstname}} {{$m->lastname}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                                                     
                         </div>     
                            <div class="col-md-6 col-md-offset-3">
                                <button type="button" class="btn btn-warning mr-1">
                                    <i class="ft-x"></i> Cancel
                                </button>
                                <button type="submit" class="btn btn-primary" id="savebtn">
                                    <i class="fa fa-check-square-o"></i>Add Group <img src="{{asset('assets/images/loading.gif')}}" style="max-height: 20px;display: none;" id="loading">
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('custom_scripts')

<script src="{{asset('assets/bower_components/select2/dist/js/core/select2.full.min.js')}}"></script>
  <script type="text/javascript">
     $(".select2").select2();
     </script>

@endsection