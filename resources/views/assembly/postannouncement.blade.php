@extends('layouts.partials.mainlayout')
@section('content')
<div class="row match-height">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">Post Announcement</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                    @endif
                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{Session::get('error')}}
                    </div>
                    @endif
                    <div class="card-text">
                    </div>
                    <form class="form" id="addnewsform" method="post" action="{{url('/saveannouncement')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="save_ann" value="{{Session::get('assembly_id')}}">
                        <div class="form-body">
                            <h4 class="form-section"></h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" id="title" class="form-control" placeholder="Announcement Title" name="title">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="content">Content</label>
                                        <textarea class="form-control" rows="4" name="description" id="editor1"></textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="class">Featured Image:</label>
                                        <input type="text" class="form-control image" name="featured_image" placeholder="Click to add Image " id="addImageUrl" readonly="readonly" value="" onfocus="this.blur()" onclick="openCustomRoxy2('addImageUrl')" />
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <img class="card-img-top img-fluid box-shadow-4" id="addImageUrlimage" style="max-width:200px;max-height:200px" src="{{asset('assets/images/default.jpg')}}" alt="Card image cap">
                                </div>

                                <div class="col-md-3">
                                    <label for="class">Post To:</label>
                                    <fieldset>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="receivers[]" value="head_office">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Head Office Staff</span>
                                        </label>
                                    </fieldset>
                                    <fieldset>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="receivers[]" value="members">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Members</span>
                                        </label>
                                    </fieldset>
                                    <fieldset>
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="receivers[]" value="visitors">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Visitors</span>
                                        </label>
                                    </fieldset>
                                </div>
                            </div>

                        </div>


                        <div class="form-actions center">
                            <a href="{{url()->previous()}}" class="btn btn-warning mr-1">
                                <i class="ft-x"></i> Cancel
                            </a>
                            <button type="submit" class="btn btn-primary" id="savebtn">
                                <i class="fa fa-check-square-o"></i> Post Announcement <img src="{{asset('assets/images/loading.gif')}}" style="max-height: 20px;display: none;" id="loading">
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts-below')
<script src="{{asset('assets/vendors/js/extensions/sweetalert.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/bower_components/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    CKEDITOR.replace('editor1', {
        filebrowserBrowseUrl: '{{Request::root().' / '.Session::get('
        assembly_id ').' / index.html ? integration = ckeditor '}}',
        filebrowserImageBrowseUrl : '{{Request::root().' / '.Session::get('
        assembly_id ').' / index.html ? integration = ckeditor '}}',
        filebrowserWindowWidth : '1000',
        filebrowserWindowHeight: '700'
    });

    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
</script>
@endsection