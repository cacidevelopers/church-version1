<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Christ Apostolic Church International</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('/assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('/assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('/assets/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('/assets/dist/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('/assets/plugins/iCheck/square/blue.css')}}">

  
  <style>
  .login-box, .register-box {
    width: 600px;
    margin: 7% auto;
    

}

.login-page, .register-page {
    /* background: #fff; */
    background: url('{{ asset("img/cry.jpg") }}') no-repeat center fixed !important;
    background-size: cover !important;

}
.login-logo a, .register-logo a {
    color: #fff;
}
.login-box-body, .register-box-body {
    background: #fff;
    border-radius: 45px;
    padding: 60px;
    border-top: 0;
    color: #666;
    opacity: 0.8;
    
}
.form-control {
    border-radius: 10px;
    box-shadow: 1px 1px #000;
    border-color: #d2d6de;
    padding: 20px;
}
html, body {
    height: fit-content;
}
  </style>

 

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{asset('/assets/index2.html')}}"><b>Christ Apostolic Church International</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <h3 class="login-box-msg">Sign in to start your session</h3>

      @if (Session::has('error'))
      <div class="alert bg-danger alert-dismissible mb-2" role="alert" id="error">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>My friend! </strong> Spell your name well!! Password too, you have forgotten!!
                </div>
            @endif
    <form action="{{url('/login')}}" method="post">
      {{ csrf_field() }}
    <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" placeholder="Email or Phone Number">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      
    </div>
    <!-- /.social-auth-links -->
  <h4><u>
    <a href="{{url('/forgotpassword')}}">I forgot my password</a><br>
    <a href="{{url('/register')}}" class="text-center">Register a new membership</a>
<u></h4>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{asset('/assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('/assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('/assets/plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
  setTimeout(function(){$('#error').slideUp(500)},5000);
</script>
</body>
</html>
