<!DOCTYPE html>
    <html lang="en">
     <head>
     @yield('css-above')
       @include('layouts.partials.head')
     </head>

     <body>
    
    @include('layouts.partials.header')
    @include('layouts.partials.nav')
    
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
     
    </section>

    <!-- Main content -->
    <section class="content">
    @yield('content')

    </section>
    <!-- /.content -->
  </div>


    @include('layouts.partials.footer')
    @include('layouts.partials.footer-scripts')

@yield('custom_scripts')
     </body>
    </html>