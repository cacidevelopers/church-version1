<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
   
    <!-- <a href="https://cac-int.org/" class="btn btn-block btn-social btn-facebook btn-flat"><i class=""></i> 
The Christ Apostolic Church International: Enterprise Resource Planner: [CACISoft version 1.0.0]</a>  -->

<strong>Copyright &copy; 2019-2020 <a href="https://cac-int.org/">The Christ Apostolic Church International:</a>.</strong> All rights
    reserved.
  </footer>